coreo_aws_rule "s3-encryption" do
  action :define
  service :s3
  link "http://kb.cloudcoreo.com/mydoc_all-inventory.html"
  include_violations_in_count false
  display_name "S3 Inventory"
  description "This rule performs an inventory on all S3 buckets in the target AWS account."
  category "Inventory"
  suggested_action "None."
  level "Informational"
  objectives  [ "list_buckets", "list_objects_v2", "head_object" ]
  call_modifiers [ "", { bucket: "objective[0].name" }, { bucket: "objective[0].name", key: "objective[1].contents[0].key" } ]
  audit_objects [ "", "", "server_side_encryption" ]
  operators [ "", "", "=~" ]
  raise_when [ "", "", // ]
  id_map "object.buckets.name"
end
